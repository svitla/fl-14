function changeColor() {
    document.addEventListener('click', event => {
        if (clickInsideElement(event, 'cell')) {
            let tar = event.target;
            if (tar.classList.contains('special')) {
                let parentTable = tar.parentNode.parentNode;
                parentTable.style.backgroundColor = 'green';
                return;
            }
            if (tar.classList.contains('first')) {
                let parentRow = tar.parentNode;
                parentRow.style.backgroundColor = 'blue';
                return;
            }
            tar.style.backgroundColor = 'yellow';
        }
    });
}

changeColor();

function clickInsideElement(e, className) {
    let el = e.target;

    if (el.classList.contains(className) === true) {
        return el;
    } else {
        while ((el = el.parentNode) !== null) {
            if (el.classList && el.classList.contains(className)) {
                return el;
            }
        }
    }
    return false;
}


function checkInput() {
    let button = document.getElementsByClassName('button')[0];
    let input = document.getElementsByClassName('input')[0];
    let value = input.value;
    let pattern = /^\+38(0\d{9})$/;

    if (!pattern.test(value)) {
        let messageDiv = document.getElementById('message');
        let messageP = document.getElementById('message_inner');

        messageDiv.style.display = 'block';
        messageP.innerHTML = 'Type number does not follow format +380*********';
        messageDiv.style.backgroundColor = 'pink';
        input.style.border = '2px solid pink';
        button.setAttribute('disabled', 'true');
    } else {
        let messageDiv = document.getElementById('message');
        messageDiv.style.display = 'none';
        button.removeAttribute('disabled');
        input.style.border = '2px solid black';
    }

    button.addEventListener('click', () => {
        let messageDiv = document.getElementById('message');
        let messageP = document.getElementById('message_inner');

        messageDiv.style.display = 'block';
        messageP.innerHTML = 'Data was successfully sent';
        messageDiv.style.backgroundColor = 'green';
    });
}


function moveBall() {
    let task = document.getElementById('task3');
    let message = document.getElementById('scoreMessage');
    let ball = document.getElementById('ball');
    let court = document.getElementById('court');

    court.addEventListener('click', (event) => {

        let scoreA = document.getElementsByClassName('num')[0];
        let scoreB = document.getElementsByClassName('num')[1];

        let x = event.offsetX;
        let y = event.offsetY;
        let left;
        let top;

        const twenty = 20;
        const maxRight = 580;
        const fourty = 40;
        const maxBottom = 310;

        if (x < twenty) {
            left = x;
        } else if (x > maxRight) {
            left = x - fourty;
        } else {
            left = x - twenty;
        }

        if (y < twenty) {
            top = y;
        } else if (y > maxBottom) {
            top = y - fourty;
        } else {
            top = y - twenty;
        }

        const sec = 3000;
        task.addEventListener('scoreMessage', function (event) {
            message.style.display = 'block';
            message.innerHTML = event.detail.title + ' score!';
            setTimeout(() => {
                message.style.display = 'none';
            }, sec);
        });

        const secondLeftB = 557;
        const secondRightB = 574;
        const topB = 172;
        const bottomB = 155;
        const firstLeftB = 25;
        const firstRightB = 42;

        if (x > secondLeftB && x < secondRightB && y > bottomB && y < topB) {
            task.dispatchEvent(new CustomEvent('scoreMessage', {
                detail: {title: 'Team A'}
            }));
            scoreA.innerHTML = parseInt(scoreA.innerHTML) + 1;
        }
        if (x > firstLeftB && x < firstRightB && y > bottomB && y < topB) {
            task.dispatchEvent(new CustomEvent('scoreMessage', {
                detail: {title: 'Team B'}
            }));
            scoreB.innerHTML = parseInt(scoreB.innerHTML) + 1;
        }

        if (event.target.id === 'ball') {
            return;
        }

        ball.style.left = left + 'px';
        ball.style.top = top + 'px';
        ball.style.transition = 'all .7s';
    });
}
moveBall();