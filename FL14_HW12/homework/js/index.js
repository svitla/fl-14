function visitLink(path) {
    let clickNum = localStorage.getItem(path);
    clickNum = clickNum ? parseInt(clickNum) : 0;
    clickNum = ++clickNum;
    let a = clickNum.toString();
    localStorage.setItem(path, a);
}

function viewResults() {
    let div = document.getElementById('content');
    if (div.lastElementChild.tagName === 'UL' || div.lastElementChild.tagName === 'P') {
        div.removeChild(div.lastElementChild);
    }

    let ul = document.createElement('ul');
    div.append(ul);
    console.log(localStorage);

    if (localStorage.length == 0){
        let p = document.createElement('p');
        div.append(p);
        p.innerHTML = 'No pages visited';
    }

    let keys = Object.keys(localStorage);
    for (let j = 0; j < keys.length; j++) {
        let li = document.createElement('li');
        ul.append(li);
        li.innerHTML = 'You visited ' + keys[j] + " " + localStorage.getItem(keys[j]) + ' time(s)';
    }
    localStorage.clear();
}




