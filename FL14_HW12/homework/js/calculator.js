function calculate() {
    try{
        let expr = prompt("Input your math expression");
        if(expr == null) {
            return;
        }
        let result = eval(expr.replace(/\s/g, ''));
        if (isNaN(result)){
            throw new EvalError("The result is not a number")
        }
        if (result===undefined){
            throw new EvalError("The result is undefined")
        }
        alert(result);
    } catch (e) {
        alert(e.message + '. Try again.');
        calculate();
    }
}

calculate();