let questionsVar = window.questions;
let arrayQuestions;
let startButton = document.getElementById('startGame');
let skipButton = document.getElementById('skipQuestion');
let blockQ = document.getElementById('blockQuestion');
let blockAns1 = document.getElementById('answer1');
let blockAns2 = document.getElementById('answer2');
let blockAns3 = document.getElementById('answer3');
let blockAns4 = document.getElementById('answer4');
let blockMessage = document.getElementById('showMessage');
let blockGame = document.getElementById('game');
let blockTotal = document.getElementById('totalPrize');
let blockCurrent = document.getElementById('currentPrize');
let number;
let totalAmount;
let currentAmount;
let prize;
let double = 2;
let final = 1000000;
let startCurrent = 100;

let questionObject;

function updateQuestion(){
    number = Math.floor(Math.random() * arrayQuestions.length);
    showQuestion();
}

function updatePrize() {
    prize = totalAmount;

    blockTotal.innerHTML = totalAmount;
    blockCurrent.innerHTML = currentAmount;
    totalAmount += currentAmount;
    currentAmount *= double;
    if (prize >= final){
        blockGame.setAttribute('style', 'display:none;')
        blockMessage.innerHTML = 'Congratulations! You won 1000000';
        startButton.removeAttribute('disabled');
    }
}


function showQuestion() {
    questionObject = arrayQuestions[number];
    arrayQuestions.splice(number,1);

    let first = 0;
    let second = 1;
    let third = 2;
    let fourth = 3;

    blockQ.innerHTML = questionObject.question;
    blockAns1.innerHTML = questionObject.content[first];
    blockAns2.innerHTML = questionObject.content[second];
    blockAns3.innerHTML = questionObject.content[third];
    blockAns4.innerHTML = questionObject.content[fourth];
}

function checkAnswer(value) {
    if (value === questionObject.correct){
        updateQuestion();
        updatePrize();
    } else{
        blockGame.setAttribute('style', 'display:none;')
        blockMessage.innerHTML = 'Game is over. Your prize is ' + prize;
        startButton.removeAttribute('disabled');
    }
}

function startGame() {
    arrayQuestions = questionsVar.slice();
    totalAmount = 0;
    currentAmount = startCurrent;
    blockGame.style.display = 'flex';
    blockMessage.innerHTML = '';
    startButton.setAttribute('disabled', 'true');
    skipButton.removeAttribute('disabled');

    updateQuestion();
    updatePrize();
}

function skipQuestion() {
    skipButton.setAttribute('disabled', 'true');
    updateQuestion();
}

