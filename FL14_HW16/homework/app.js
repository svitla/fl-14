
(function mounted() {
    getTableData();
})();

function guid() {
    return parseInt(Date.now() + Math.random());
}

function saveMemberInfo() {
    let keys = ['book', 'author'];
    let obj = {};

    keys.forEach(function (item, index) {
        let result = document.getElementById(item).value;
        if (result) {
            obj[item] = result;
        }
    })

    if (Object.keys(obj).length) {
        let members = getMembers();
        obj.id = guid();
        members.push(obj);
        var data = JSON.stringify(members);
        localStorage.setItem("members", data);
        clearFields();
        insertIntoTableView(obj, getTotalRowOfTable());
        $('#addnewModal').modal('hide')
    }
}

function clearFields() {
    $('#input_form')[0].reset();
}

function getMembers() {
    let memberRecord = localStorage.getItem("members");
    let members = [];
    if (!memberRecord) {
        return members;
    } else {
        members = JSON.parse(memberRecord);
        return members;
    }
}

function getFormattedMembers() {
    var members = getMembers();

    return members;

}
function getTableData() {
    $("#member_table").find("tr:not(:first)").remove();

    let searchKeyword = $('#member_search').val();
    let members = getFormattedMembers();



    members.forEach(function (item, index) {
        insertIntoTableView(item, index + 1);
    })
}

function insertIntoTableView(item, tableIndex) {
    let table = document.getElementById('member_table');
    let row = table.insertRow();
    let idCell = row.insertCell(0);
    let bookCell = row.insertCell(1);
    let authorCell = row.insertCell(2);
    let actionCell = row.insertCell(3);

    idCell.innerHTML = tableIndex;
    bookCell.innerHTML = item.book;
    authorCell.innerHTML = item.author;
    var guid = item.id;

    actionCell.innerHTML = '<button class="btn btn-sm btn-default" onclick="showMemberData(' + guid + ')">View</button> ' +
        '<button class="btn btn-sm btn-primary" onclick="showEditModal(' + guid + ')">Edit</button> ' +
        '<button class="btn btn-sm btn-danger" onclick="showDeleteModal(' + guid + ')">Delete</button>';
}


function getTotalRowOfTable() {
    let table = document.getElementById('member_table');
    return table.rows.length;
}

function showMemberData(id) {
    let allMembers = getMembers();
    let member = allMembers.find(function (item) {
        return item.id == id;
    })

    $('#show_book').val(member.book);
    $('#show_author').val(member.author);

    $('#showModal').modal();

}


function showEditModal(id) {
    let allMembers = getMembers();
    let member = allMembers.find(function (item) {
        return item.id == id;
    })

    $('#edit_book').val(member.book);
    $('#edit_author').val(member.author);
    $('#member_id').val(id);

    $('#editModal').modal();
}


function updateMemberData() {

    let allMembers = getMembers();
    let memberId = $('#member_id').val();

    let member = allMembers.find(function (item) {
        return item.id == memberId;
    })

    member.book = $('#edit_book').val();
    member.author = $('#edit_author').val();


    var data = JSON.stringify(allMembers);
    localStorage.setItem('members', data);

    $("#member_table").find("tr:not(:first)").remove();
    getTableData();
    $('#editModal').modal('hide')
}

function showDeleteModal(id) {
    $('#deleted-member-id').val(id);
    $('#deleteDialog').modal();
}

function deleteMemberData() {
    let id = $('#deleted-member-id').val();
    let allMembers = getMembers();

    let storageUsers = JSON.parse(localStorage.getItem('members'));

    let newData = [];

    newData = storageUsers.filter(function (item, index) {
        return item.id != id;
    });

    let data = JSON.stringify(newData);

    localStorage.setItem('members', data);
    $("#member_table").find("tr:not(:first)").remove();
    $('#deleteDialog').modal('hide');
    getTableData();

}
