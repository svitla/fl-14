let position;
let length;
let str = prompt("Input a word");

function task2() {
    if (!str || /^\s*$/.test(str)) {
        alert("Invalid value");
        return;
    }

    if (str.length % 2 == 1) {
        position = str.length / 2;
        length = 1;
    } else {
        position = str.length / 2 - 1;
        length = 2;
    }
    let result = str.substring(position, position + length);

    if (result.length > 1 && result[0] === result[1]) {
        alert("Middle characters are the same")
        return;
    } else {
        alert(result);
    }
}
task2();