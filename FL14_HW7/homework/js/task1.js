let amount = prompt('Put amount of batteries');
let percentage = prompt('Put percentage of defective batteries');

function task1() {
    let n = parseInt(amount);
    let p = parseInt(percentage);
    let hundred = 100;

    let defective = n * p / hundred;
    let working = n - defective;


    if (isNaN(n) || amount < 0 || isNaN(p) || percentage < 0 || percentage > hundred) {
        alert('Invalid input data');
    } else {
        alert('Amount of batteries: ' + n +
            '\nDefective rate: ' + p + '%' +
            '\nAmount of defective batteries: ' + Math.round((defective + Number.EPSILON) * hundred) / hundred +
            '\nAmount of working batteries: ' + Math.round((working + Number.EPSILON) * hundred) / hundred
        );
    }
}

task1();

