const days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];

function getAge(dateString) {
    let today = new Date(),
        birthDate = new Date(dateString);
    let age = today.getFullYear() - birthDate.getFullYear();
    let m = today.getMonth() - birthDate.getMonth();
    if (m < 0 || m === 0 && today.getDate() < birthDate.getDate()) {
        age--;
    }
    return age;
}

function getWeekDay(dateString) {
    let d = new Date(dateString);
    return days[d.getDay()];
}

function getProgrammersDay(year) {
    let day,
        twnine = 29,
        twelve = 12,
        thirteen = 13,
        sep = 8;
    const isLeap = year => new Date(year, 1, twnine).getDate() === twnine;
    if (isLeap(year)) {
        day = twelve;
    } else {
        day = thirteen;
    }
    let dateString = new Date(year, sep, day);
    let weekDay = getWeekDay(dateString);
    return day + ' Sep' + ', ' + year + ' (' + weekDay + ')';
}

function howFarIs(weekDay) {
    let nowWeekNum,
        weekNum,
        number;
    let nowWeekDay = getWeekDay(new Date());

    let weekDay1 = capitalize(weekDay);

    nowWeekNum = findNum(nowWeekDay);
    weekNum = findNum(weekDay1);

    if (weekNum > nowWeekNum) {
        number = weekNum - nowWeekNum;
    } else if (weekNum < nowWeekNum) {
        number = nowWeekNum - weekNum;
    } else {
        return `Hey, today is ${weekDay1} =)`;
    }

    return `It's ${number} day(s) left till ${weekDay1}`;
}

function findNum(a) {
    let res;
    for (let i = 0; i < days.length; i++) {
        if (days[i] === a) {
            res = i;
        }
    }
    return res;
}

function isValidIdentifier(string) {
    let validVar = /^[a-zA-Z_$][a-zA-Z0-9_$]*$/;
    return validVar.test(string);
}

function capitalize(string) {
    let re = /(\b[a-z](?!\s))/g;
    string = string.replace(re, x => {
        return x.toUpperCase();
    });
    return string;
}

function isValidAudioFile(string) {
    let validAudio = /^[a-zA-Z]+(.mp3|.flac|.alac|.aac)$/;
    return validAudio.test(string);
}

function getHexadecimalColors(string) {
    let needed = /#([a-fA-F0-9]{6}|[a-fA-F0-9]{3})\b/gi ;
    let result = string.match(needed);
    if (result === null){
        return [];
    }
    return result;
}

function isValidPassword(password) {
    let validPass = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{8,}$/g;
    return validPass.test(password);
}

function addThousandsSeparators(number) {
    let str = number.toString();
    let pattern = /\B(?=(\d{3})+(?!\d))/g;
    return str.replace(pattern, ',');
}