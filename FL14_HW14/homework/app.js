const data = [
    {
        'folder': true,
        'title': 'Grow',
        'children': [
            {
                'title': 'logo.png'
            },
            {
                'folder': true,
                'title': 'English',
                'children': [
                    {
                        'title': 'Present_Perfect.txt'
                    }
                ]
            }
        ]
    },
    {
        'folder': true,
        'title': 'Soft',
        'children': [
            {
                'folder': true,
                'title': 'NVIDIA',
                'children': null
            },
            {
                'title': 'nvm-setup.exe'
            },
            {
                'title': 'node.exe'
            }
        ]
    },
    {
        'folder': true,
        'title': 'Doc',
        'children': [
            {
                'title': 'project_info.txt'
            }
        ]
    },
    {
        'title': 'credentials.txt'
    }
];

const rootNode = document.getElementById('root');
let target;

let container;
container = document.createElement('div');
container.classList.add('container');
rootNode.appendChild(container);

let ulFirst;
ulFirst = document.createElement('ul');
ulFirst.classList.add('ul');
container.appendChild(ulFirst);

let objects = new Map();
let parents = new Map();

let arrayToDelete;
let indexToDelete;
let focused;

function showElementsOfArray(array, ulFirst) {
    let row, icon, title, row1;

    let i = -1;
    array.forEach(el => {
        i++;

        row = document.createElement('li');
        row.classList.add('row');
        ulFirst.appendChild(row);
        objects.set(row, i);
        parents.set(row, array);

        const rowInner = document.createElement('div');
        rowInner.classList.add('row_inner');
        row.appendChild(rowInner);

        icon = document.createElement('i');
        icon.classList.add('material-icons');
        rowInner.appendChild(icon);

        if (el.folder === true) {
            icon.classList.add('folder');
            icon.innerHTML = 'folder';
        } else {
            icon.classList.add('file');
            icon.innerHTML = 'insert_drive_file';
        }

        title = document.createElement('p');
        title.classList.add('title');
        rowInner.appendChild(title);
        title.innerHTML = el.title;


        if (el.children !== undefined) {
            const ul = document.createElement('ul');
            ul.classList.add('ul', 'nested');
            if (el.open) {
                ul.classList.add('active');
                icon.innerHTML = 'folder_open';
            }
            row.appendChild(ul);

            rowInner.addEventListener('click', function () {
                el.open = el.open !== true;
                ul.classList.toggle('active');

                if (ul.previousSibling.children[0].innerHTML === 'folder') {
                    ul.previousSibling.children[0].innerHTML = 'folder_open';
                } else if (ul.previousSibling.children[0].innerHTML === 'folder_open') {
                    ul.previousSibling.children[0].innerHTML = 'folder';
                }
            });

            if (el.children === null || el.children.length === 0) {
                row1 = document.createElement('li');
                row1.classList.add('row1');
                ul.appendChild(row1);

                row1.innerHTML = 'No items';
                row1.style.fontStyle = 'italic';
                return;
            }
            showElementsOfArray(el.children, ul);
        }
    });
}


function createCustomMenu() {
    let menu, ul, li1, li2;

    menu = document.createElement('div');
    menu.classList.add('menu');

    ul = document.createElement('ul');
    ul.classList.add('menu_ul');
    menu.appendChild(ul);

    li1 = document.createElement('li');

    li1.addEventListener('click', () => {
        let input = document.createElement('input');
        input.classList.add('edit_input');
        input.setAttribute('value', focused.children[1].innerHTML);

        input.onclick = function(e) {
            arrayToDelete[indexToDelete].title = input.value;
            e.stopPropagation();
            refresh();
        };

        input.addEventListener('blur', () => {
            arrayToDelete[indexToDelete].title = input.value;
            refresh();
        });

        const enter = 13;
        input.onkeydown = function (e) {
            if (e.keyCode === enter) {
                arrayToDelete[indexToDelete].title = input.value;
                refresh();
            }
        };


        focused.children[1].innerHTML = '';
        focused.appendChild(input);
        input.focus();

        let val = input.value;
        let needToSelect = val.match(/([^.]+)/g);
        let chars = needToSelect[0].split('');
        input.setSelectionRange(0,chars.length);

    }, false);
    li1.classList.add('menu_li');
    ul.appendChild(li1);
    li1.innerHTML = 'Edit';

    li2 = document.createElement('li');
    li2.onclick = () => {
        arrayToDelete.splice(indexToDelete, 1);
        refresh();
    };
    li2.classList.add('menu_li');
    ul.appendChild(li2);
    li2.innerHTML = 'Delete';

    rootNode.appendChild(menu);
}

function refresh() {
    objects = new Map();
    parents = new Map();
    ulFirst.innerHTML = '';
    showElementsOfArray(data, ulFirst);

}

function getRow(el) {
    let curr = el;
    while (curr.nodeName !== 'LI') {
        curr = curr.parentNode;
    }
    return curr;
}


function toggleCustomMenu() {
    let divEl;

    container.addEventListener('contextmenu', event => {
            createCustomMenu();
            const menu = document.querySelector('.menu');
            event.preventDefault();

            target = event.target.nodeName;
            if (focused !== undefined) {
                focused.style.backgroundColor = 'transparent';
            }

            menu.style.top = `${event.clientY}px`;
            menu.style.left = `${event.clientX}px`;

            if (clickInsideElement(event, 'folder') || clickInsideElement(event, 'row_inner')
                || clickInsideElement(event, 'title')) {
                if (menu.classList.contains('disable')){
                    menu.classList.remove('disable');
                }
                menu.classList.add('active');
            } else {
                menu.classList.add('active', 'disable');
                return;
            }

            divEl = getRow(event.target);
            focused = divEl.children[0];
            focused.style.backgroundColor = 'lightskyblue';

            indexToDelete = objects.get(divEl);
            arrayToDelete = parents.get(divEl);
        }
    );

    document.addEventListener('click', event => {
        const menu = document.querySelector('.menu');
        let two = 2;
        if (event.button !== two) {
            menu.classList.remove('active');
            if (focused !== undefined) {
                focused.style.backgroundColor = 'transparent';
            }
        }
    }, false);
}

function clickInsideElement(e, className) {
    let el = e.target;

    if (el.classList.contains(className) === true) {
        return el;
    } else {
        while ((el = el.parentNode) !== null) {
            if (el.classList && el.classList.contains(className)) {
                return el;
            }
        }
    }
    return false;
}


showElementsOfArray(data, ulFirst);
createCustomMenu();
toggleCustomMenu();

