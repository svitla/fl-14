function isEquals(a, b) {
    return a === b;
}

function numberToString(a) {
    return a.toString();
}

function storeNames() {
    return Array.prototype.slice.call(arguments, 0);
}

function getDivision(a, b) {
    if (a > b) {
        return a / b;
    } else {
        return b / a;
    }
}

function negativeCount(array) {
    let result = 0;
    array.forEach(el => {
        if (el < 0) {
            result++;
        }
    });
    return result;
}

function letterCount(word, letter) {
    let result = 0;
    let chars = word.split('');
    chars.forEach(el => {
        if (el === letter) {
            result++;
        }
    });
    return result;
}

function countPoints(collection) {
    let result = 0;
    let win = 3;
    collection.forEach(el => {
        let round = el.split(':');
        round = round.map(x => parseInt(x));
        if (round[0] > round[1]) {
            result += win;
        } else if (round[0] === round[1]) {
            result += 1;
        }
    });
    return result;
}