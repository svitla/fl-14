/*Task 1*/
function convert(array) {
    let result = [];
    for (let i = 0; i < array.length; i++) {
        if (typeof array[i] === 'string') {
            let a = parseInt(array[i]);
            result.push(a);
        } else if (typeof array[i] === 'number') {
            let a = array[i] + '';
            result.push(a);
        } else {
            console.log('Invalid data');
        }
    }
    return result;
}

/* Task 2*/
function executeForEach(array, func) {
    for (let i = 0; i < array.length; i++) {
        func(array[i]);
    }
}

/* Task 3 */
function mapArray(array, func) {
    let result = [];
    executeForEach(array, function (el) {
        const a = typeof el === 'string'
            ? parseInt(el)
            : el;
        result.push(func(a));
    });
    return result;
}

/* Task 4 */
function filterArray(array, func) {
    let result = [];
    executeForEach(array, function (el) {
        const a = func(el);
        if (a) {
            result.push(el);
        }
    });
    return result;
}

/* Task 5 */
function getValuePosition(array, n) {
    let result;
    for (let i = 0; i < array.length; i++) {
        if (array[i] === n) {
            result = i + 1;
            return result;
        }
    }
    if (result === undefined) {
        return false;
    }
    return result;
}

/* Task 6 */
function flipOver(str) {
    let result = '';
    for (let i = str.length - 1; i >= 0; i--) {
        result += str.charAt(i);
    }
    return result;
}

/* Task 7 */
function makeListFromRange(array) {
    let result = [];
    if (array[0] < array[1]) {
        for (let i = array[0]; i <= array[1]; i++) {
            result.push(i);
        }
    } else {
        for (let i = array[0]; i >= array[1]; i--) {
            result.push(i);
        }
    }
    return result;
}

/* Task 8 */
function getArrayOfKeys(array, key) {
    let result = [];
    executeForEach(array, function (el) {
        result.push(el[key]);
    });
    return result;
}

/* Task 9 */
function getTotalWeight(array) {
    let weight = 0;
    executeForEach(array, function (el) {
        weight += el['weight'];
    });
    return weight;
}

/* Task 10 */
function getPastDay(date, numOfDays) {
    let newDate = new Date(date);
    let result = newDate.getDate() - numOfDays;
    newDate.setDate(result);
    newDate = newDate.getDate();
    return newDate;
}

/* Task 11 */
function formatDate(d) {
    let ten = 10;
    function pad(n) {
        return n < ten ? '0' + n : n
    }
    return d.getUTCFullYear()+'/'
        + pad(d.getUTCMonth()+1)+'/'
        + pad(d.getDate())+' '
        + pad(d.getHours())+':'
        + pad(d.getMinutes())
}
