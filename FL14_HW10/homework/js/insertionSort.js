
//
console.log('Insertion sort');

function insertionSort(arr){
    let i, len = arr.length, el, j;

    for(i = 1; i<len; i++){
        el = arr[i];
        j = i-1;

        while(j>=0 && arr[j]>=el){
            arr[j+1] = arr[j];
            j--;
        }

        arr[j+1] = el;
    }

    return arr;
}


